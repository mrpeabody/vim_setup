{
  "parserOptions": {
  "ecmaVersion": 7,
  "sourceType": "module",
    "ecmaFeatures": {
      "jsx": true
    }
  },
  "env": {
    "browser": true,
    "jquery": true,
    "node": true
  },
  "globals": {
    "define": false,
    "d3": false,
    "md5": false,
    "$$": false,
    "webix": false,
    "Promise": true,
    "Highcharts": true
  },
  "settings": {
    "ecmascript": 6,
    "jsx": true
  },
  "plugins": [
    "react"
  ],
  "rules": {
    "no-bitwise": 0,
    "no-cond-assign": 0,
    "camelcase": 0,
    "curly": 1,
    "no-debugger": 0,
    "no-sparse-arrays": 0,
    "eqeqeq": 2,
    "no-eq-null": 0,
    "no-eval": 0,
    "no-unused-expressions": 0,
    "guard-for-in": 1,
    "no-extend-native": 0,
    "block-scoped-var": 0,
    "strict": 0,
    "wrap-iife": 0,
    "no-iterator": 0,
    "no-use-before-define": 0,
    "linebreak-style": 0,
    "comma-style": [
      1,
      "last"
    ],
    "no-loop-func": 0,
    "indent": [
      1,
      4,
      { "SwitchCase": 1, "ObjectExpression": 1 }
    ],
    "max-len": [
      1,
      {
        "code": 120,
        "ignoreComments": true
      }
    ],
    "no-multi-str": 0,
    "new-cap": 0,
    "no-caller": 2,
    "no-sequences": 0,
    "no-empty": 2,
    "no-irregular-whitespace": 1,
    "no-new": 2,
    "valid-typeof": 0,
    "require-yield": 0,
    "no-plusplus": 0,
    "no-proto": 0,
    "quotes": [
      1,
      "single"
    ],
    "space-infix-ops": [1, {"int32Hint": true}],
    "key-spacing": [1, {
        "singleLine": {
            "beforeColon": false,
            "afterColon": true,
            "mode": "strict"
        },
        "multiLine": {
            "beforeColon": false,
            "afterColon": true,
            "mode": "strict"
        }
    }],
    "keyword-spacing": [1, {
        "before": true,
        "after": true
    }],
    "space-before-function-paren": [1, {
        "anonymous": "never",
        "named": "never",
        "asyncArrow": "always"
    }],
    "space-before-blocks": [1, "always"],
    "no-script-url": 0,
    "no-shadow": 2,
    "dot-notation": 1,
    "no-new-func": 0,
    "no-new-wrappers": 0,
    "no-undef": 2,
    "no-unused-vars": 1,
    "no-invalid-this": 0,
    "no-var": 0,
    "no-with": 0,
    "jsx-quotes": [1, "prefer-double"],
    "react/display-name": 0,
    "react/forbid-prop-types": 0,
    "react/jsx-boolean-value": 0,
    "react/jsx-closing-bracket-location": 1,
    "react/jsx-curly-spacing": 1,
    "react/jsx-handler-names": 1,
    "react/jsx-indent-props": 1,
    "react/jsx-indent": 1,
    "react/jsx-key": 1,
    "react/jsx-max-props-per-line": 0,
    "react/jsx-no-bind": 0,
    "react/jsx-no-duplicate-props": 1,
    "react/jsx-no-literals": 0,
    "react/jsx-no-undef": 1,
    "react/jsx-pascal-case": 1,
    "react/jsx-sort-prop-types": 0,
    "react/jsx-sort-props": 0,
    "react/jsx-uses-react": 1,
    "react/jsx-uses-vars": 1,
    "react/no-danger": 1,
    "react/no-deprecated": 1,
    "react/no-did-mount-set-state": 0,
    "react/no-did-update-set-state": 1,
    "react/no-direct-mutation-state": 1,
    "react/no-is-mounted": 1,
    "react/no-multi-comp": 0,
    "react/no-set-state": 0,
    "react/no-string-refs": 0,
    "react/no-unknown-property": 1,
    "react/prefer-es6-class": 1,
    "react/prop-types": 0,
    "react/react-in-jsx-scope": 1,
    "react/self-closing-comp": 1,
    "react/sort-comp": 0,
    "react/jsx-equals-spacing": [1, "never"]
  }
}
